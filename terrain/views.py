from django.views.generic import TemplateView
from terrain.grid import (
    create_matrix_with_node_instances,
    generate_least_cost_matrix,
    generate_least_cost_path
)


class TerrainView(TemplateView):
    template_name = 'index.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        matrix = create_matrix_with_node_instances()
        least_cost_matrix = generate_least_cost_matrix(matrix)
        least_cost_path, least_cost_points = generate_least_cost_path(least_cost_matrix)

        context['matrix'] = matrix
        context['least_cost_matrix'] = least_cost_matrix
        context['least_cost_path'] = least_cost_path
        context['least_cost_points'] = least_cost_points

        return context
