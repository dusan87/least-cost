import random

GRID_DIMENSION = 6


class Node(object):
    def __init__(self, hex_value):
        self.hex_value = hex_value
        self.value = int(hex_value, 16)

    def __str__(self, ):
        return "{} ({})".format(self.hex_value, self.value)


def generate_hex():
    return '{:03x}'.format(random.randrange(16 ** 3)).upper()


def generate_hex_grid():
    hex_grid = []
    for _ in range(0, GRID_DIMENSION):
        hex_grid.append([generate_hex() for _ in range(0, 6)])
    return hex_grid


def create_matrix_with_node_instances():
    matrix = []
    hex_grid = generate_hex_grid()

    for i in range(0, len(hex_grid)):
        matrix.append([Node(hex_value) for hex_value in hex_grid[i]])

    return matrix


def generate_least_cost_matrix(matrix):
    """
    It generates least cost matrix according to given matrix (terrain hex grid).
    We use following logic:
     - Temporary matrix populate with all zero values
     - Start point (top left) from original matrix assign to temporary matrix start point.
     - From start point we can move only right and down. Thus, sump up next right/down value with predecessor value.
     - Main function - For each next node we look up whether its up or left neighbour's value (cost) is lower.
       So, we choose lower cost and sum with current node value.

    :param matrix: list of lists. where each list element is an instance of Node class.
    :return: calculated least cost matrix with sum up values
    """
    matrix_length = len(matrix)

    # populate temporary matrix with zero values
    least_cost = [[0 for _ in range(0, matrix_length)] for _ in range(0, matrix_length)]

    # initial top left value
    least_cost[0][0] = matrix[0][0].value

    # initial first row (right), first column (down) sum current with predecessor value.
    for i in range(1, matrix_length):
        least_cost[i][0] = least_cost[i - 1][0] + matrix[i][0].value
        least_cost[0][i] = least_cost[0][i - 1] + matrix[0][i].value

    # main logic / function - choose whether to go down or right according to lower left/up neighbour's value.
    for i in range(1, matrix_length):
        for j in range(1, matrix_length):
            least_cost[i][j] = matrix[i][j].value + min(least_cost[i - 1][j], least_cost[i][j - 1])

    return least_cost


def generate_least_cost_path(least_cost_matrix):
    """
    It goes from bottom right node in reverse way through least cost matrix.
    :param least_cost_matrix: calculated least cost matrix
    :return: least cost path
    """
    last_node_index = len(least_cost_matrix) - 1
    i, j = last_node_index, last_node_index
    path = []
    points = []

    while True:
        points.append((i, j))

        if i == 0 and j == 0:
            break

        if i == 0:
            j -= 1
            path.append('R')
        elif j == 0:
            i -= 1
            path.append('D')
        elif least_cost_matrix[i - 1][j] < least_cost_matrix[i][j - 1]:
            i -= 1
            path.append('D')
        else:
            j -= 1
            path.append('R')

    return ", ".join(path[::-1]), points[::-1]
