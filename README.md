# README #

Calculate Dynamic Least Cost Path

Project scope:

- Generate hex grid like matrix
- According to the original matrix discover least cost path from top left to bottom right. A user can move only right or down.

### Project stack ###
* Python 3.6 (it works on all the other python versions 2.7+)
* Django 1.11

### Getting up and running ###

The steps below will get you up and running with a local development environment. I assume you have the following installed:

* pip
* virtualenv

Clone the project

    $ git clone git@bitbucket.org:dusan87/least-cost.git 

First make sure to create and activate a _virtualenv_ (http://docs.python-guide.org/en/latest/dev/virtualenvs/)

    $ pip install virtualenv
    $ cd /path/to/mathspace/
    $ virtualenv mathspace-env
    $ source mathspace-env/bin/activate

then install the requirements for local development:

    $ pip install -r requirements.txt

You can now run the ``application`` command::

    $ python manage.py runserver 0.0.0.0:8000
    
You should be able to send the request on endpoint:

    http://127.0.0.1:8000/